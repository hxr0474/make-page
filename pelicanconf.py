#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import yaml
import os

with open("../report-tracker.yml", "r") as fh:
    meta = yaml.load(fh, Loader=yaml.SafeLoader)

AUTHOR = meta["authors"][0]
SITENAME = meta["short-title"][0:40]
SITEURL = ""
THEME="blue-penguin"

PATH = ".."
OUTPUT_PATH = "../public"

TIMEZONE = "Europe/Athens"

DEFAULT_LANG = "en"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

MARKUP = ("md", "ipynb")
PLUGIN_PATHS = ["pelican-plugins"]
PLUGINS = ["ipynb2pelican"]
IGNORE_FILES = [".ipynb_checkpoints"]
STATIC_PATHS = ["data", "src", "res", "img", "input", "output"]
PAGE_EXCLUDES = ["venv", "public", "www", "make-page", "tmp", "img"]
ARTICLE_EXCLUDES = [
    "venv",
    "public",
    "www",
    "data",
    "src",
    "res",
    "make-page",
    "tmp",
    "img",
    "input",
    "output",
]
