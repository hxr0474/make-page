# from https://github.com/peijunz/ipynb2pelican
__all__ = ["reader"]
from .reader import register, ipynbReader
